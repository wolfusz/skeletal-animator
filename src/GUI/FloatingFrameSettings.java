package GUI;

import javax.swing.JFrame;

public class FloatingFrameSettings {
	public enum PinTo { Top, Bottom, Left, Right }
	
	public PinTo pinTo;
	public FloatingFrameSettings(PinTo pinTo) {
		this.pinTo = pinTo;
	}

	public int offsetX(JFrame frame) { return 0; }
	public int offsetY(JFrame frame) { return 0; }
}
