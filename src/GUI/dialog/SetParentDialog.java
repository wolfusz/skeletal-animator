package GUI.dialog;

import java.awt.Color;
import java.util.concurrent.Callable;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import GUI.BodyControl;
import GUI.controls.Dialog;
import body.Body;
import body.Segment;

public class SetParentDialog {
	private static String value = "";
	
	public static String getValue() { return value; }
	
	public static boolean show() {
		value = "";
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setOpaque(true);
		panel.setLayout(null);
		panel.setSize(100, 20);
		
		JComboBox<String> parent = new JComboBox<>();
		parent.setSize(100, 20);
		parent.addItem("None");
		String current = (String)BodyControl.getInstance().segments.getSelectedItem();
		for (String segment : Body.getInstance().getIDs())
			if (!segment.equals(current)) {
				Segment toCheck = Body.getInstance().get(segment);
				if (toCheck != null && toCheck.hasParent() && toCheck.parent.equals(current))
					continue;
				parent.addItem(segment);
			}
		panel.add(parent);
		
		return Dialog.show(panel,
		new Callable<Boolean>() {
			@Override public Boolean call() throws Exception {
				value = (String)parent.getSelectedItem();
				return !(value.isEmpty() || value.equals("None"));
			}
		}, null);
	}
}
