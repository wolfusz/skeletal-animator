package GUI.dialog;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.concurrent.Callable;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;

import GUI.controls.Dialog;

public class SegmentCreationDialog {
	private static String lastID = "";
	private static Integer lastLength = null;
	
	public static String getID() { return lastID; }
	public static Integer getLength() { return lastLength; }
	
	@SuppressWarnings("serial")
	public static boolean show() {
		lastID = "";
		lastLength = null;
		
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setOpaque(true);
		panel.setLayout(null);
		panel.setSize(100, 40);
		
		JTextField tID = new JTextField() {
			@Override protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				if (getText().isEmpty()) {
					Graphics2D g2 = (Graphics2D)g.create();
					g2.setColor(Color.DARK_GRAY);
					g2.drawString("Segment ID:", 2, 12);
					g2.dispose();
				}
			}
		};
		tID.setSize(100, 20);
		panel.add(tID);
		
		JScrollBar sLength = new JScrollBar(JScrollBar.HORIZONTAL, 50, 1, 0, 200) {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(Color.BLACK);
				g.drawString("Length: "+getValue(), 20, 15);
			}
		};
		sLength.setLocation(0, 20);
		sLength.setSize(100, 20);
		panel.add(sLength);
		
		return Dialog.show(panel,
		new Callable<Boolean>() {
			@Override public Boolean call() throws Exception {
				lastID = tID.getText();
				lastLength = sLength.getValue();
				return !tID.getText().isEmpty();
			}
		}, null);
	}
}
