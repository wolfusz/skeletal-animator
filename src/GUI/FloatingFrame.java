package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class FloatingFrame extends JFrame {
	public static int borderWidth = 2;
	
	public JPanel content;
	public FloatingFrame(int w, int h) {
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setMinimumSize(new Dimension(w+(2*borderWidth), h+(2*borderWidth)));
		setUndecorated(true);
		setLayout(null);
		((JComponent) getContentPane()).setBorder(BorderFactory.createLineBorder(Color.ORANGE, borderWidth));

		content = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2 = (Graphics2D)g.create();
				g2.setColor(Color.BLACK);
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				g2.dispose();
			}
		};
		content.setLocation(borderWidth, borderWidth);
		content.setSize(w, h);
		content.setBorder(null);
		content.setLayout(null);
		content.setBackground(Color.ORANGE);
	}
	
	protected void finalize() {
		getContentPane().add(content, BorderLayout.CENTER);
		pack();
		setVisible(true);
	}
}
