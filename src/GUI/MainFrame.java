package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

import GUI.FloatingFrameSettings.PinTo;
import body.Body;
import body.Segment;
import body.Utils;
import utils.Point;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	public static void main(String[] args) {
		try { UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); } catch (Exception e) {}
		
		// show all the windows
		getInstance().addFloatingFrame(BodyControl.getInstance(), new FloatingFrameSettings(PinTo.Left) {
			@Override public int offsetX(JFrame frame) { return 7; }
			@Override public int offsetY(JFrame frame) { return getInstance().getHeight()/2 - frame.getHeight()/2; }
		});
		getInstance().addFloatingFrame(AnimationControl.getInstance(), new FloatingFrameSettings(PinTo.Bottom) {
			@Override public int offsetX(JFrame frame) { return getInstance().getWidth()/2 - frame.getWidth()/2; }
			@Override public int offsetY(JFrame frame) { return -7; }
		});
		getInstance().updateFloatingFrames();
		
		// add some default segments
		Segment current;
		current = Body.getInstance().add("placeHolder", null, "body_arm", 0, 0, 50);
		System.out.println(current);
		current.angle = -90;
		target = current.getPin();
		Body.getInstance().add("bicepL", "placeHolder", "body_arm", 0, 0, 40);
		Body.getInstance().add("bicepR", "placeHolder", "body_arm", 0, 0, 40, 180);
		current.updateChildrens(0);
		
		for (String ID : Body.getInstance().getIDs())
			System.out.println("ID(x,y): "+ID+"("+Body.getInstance().get(ID).x+","+Body.getInstance().get(ID).y+")");

		AnimationControl.getInstance().updateControls();
		BodyControl.getInstance().segments.setSelectedIndex(-1);
		BodyControl.getInstance().segments.setSelectedIndex(0);
	}
	
	private static MainFrame instance = null;
	public static MainFrame getInstance() {
		if (instance == null)
			instance = new MainFrame();
		return instance;
	}
	
	public static String getDir(String dirName) {
		return System.getProperty("user.dir") + File.separatorChar + dirName + File.separatorChar;
	}
	
	
	public static Point position, target;
	public static String currentSegment;
	
	public JPanel panel;
	protected MainFrame() {
		setSize(600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLocation(getX(), getY()-50);
		
		floatingFrames = new HashMap<>();
		knownImages = new HashMap<>();
		
		// DRAWING
		panel = new JPanel(){
			@Override protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D canvas = (Graphics2D)g.create();
				// clear screen
				canvas.setColor(Color.WHITE);
				canvas.fill(getBounds());
				
				AffineTransform t = canvas.getTransform();
				if (position != null)
					canvas.translate(position.x, position.y);
				if (Body.getInstance() != null)
					Body.getInstance().draw(canvas);

				// draw target point
				if (target != null) {
					canvas.setColor(Color.RED);
					canvas.drawLine((int)target.x-3,(int)target.y, (int)target.x-1,(int)target.y);
					canvas.drawLine((int)target.x+1,(int)target.y, (int)target.x+3,(int)target.y);
					canvas.drawLine((int)target.x,(int)target.y-3, (int)target.x,(int)target.y-1);
					canvas.drawLine((int)target.x,(int)target.y+1, (int)target.x,(int)target.y+3);
				}
				canvas.setTransform(t);
			}
		};
		add(panel);
		
		// EVENTS
		MouseAdapter mouseEvents = new MouseAdapter() {
			@Override public void mouseReleased(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					AnimationControl.getInstance().editorMouseReleased(e);
				}
			}
			
			int deltaX, deltaY;
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					deltaX = e.getX();
					deltaY = e.getY();
				}
			}
			
			@Override public void mouseDragged(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					target = new Point(e.getX() - position.x, e.getY() - position.y);
					if (target != null) {
						if (BodyControl.targetSegment != null) {
							// rotate segment
							double before = BodyControl.targetSegment.angle;
							double after = 0;
							if (BodyControl.getInstance().IK.isSelected())
								after = BodyControl.targetSegment.IK(target);
							else
								after = BodyControl.targetSegment.pointAt(target);
							double deltaAngle = after - before;
							// and its childrens
							BodyControl.targetSegment.updateChildrens(deltaAngle);
							Body.getInstance().segments.get("placeHolder").updateChildrens();
						}
					}
				} else if (SwingUtilities.isRightMouseButton(e)) {
					deltaX -= e.getX();
					deltaY -= e.getY();
					position = position.sub(deltaX, deltaY);
					deltaX = e.getX();
					deltaY = e.getY();
				}
			}
		};
		panel.addMouseListener(mouseEvents);
		panel.addMouseMotionListener(mouseEvents);
		
		addComponentListener(new ComponentAdapter() {
			@Override public void componentResized(ComponentEvent e) {
				position = new Point(getWidth()/2, getHeight()/2);
				updateFloatingFrames();
			}
			@Override public void componentMoved(ComponentEvent e) {
				updateFloatingFrames();
			}
		});
		
		addWindowFocusListener(new WindowFocusListener() {
			@Override public void windowLostFocus(WindowEvent e) {
			}
			
			@Override public void windowGainedFocus(WindowEvent e) {
			}
		});
				
		// TIMER
		Timer timer = new Timer(10, new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				if (Body.getInstance() != null)
					Body.getInstance().update();
				
				repaint();
			}
		});
		
		// RUN
		setVisible(true);
		timer.start();
	}

	// IMAGES
	private HashMap<String, BufferedImage> knownImages;
	public BufferedImage getImage(String imgName) {
		// if already loaded then return instance
		if (knownImages.containsKey(imgName))
			return knownImages.get(imgName);
		// else load image
		BufferedImage image = null;
		try {
        	File file = new File(MainFrame.getDir("images") + imgName);
        	image = ImageIO.read(file.toURI().toURL().openStream());
			final ImageFilter filter = new RGBImageFilter() {
				public int markerRGB = new Color(255,0,255).getRGB() | 0xFFFF00FF;
				@Override public int filterRGB(int x, int y, int rgb) {
					if ((rgb | 0xFF000000) == markerRGB)
						return 0x00FFFFFF & rgb;
					else
						return rgb;
				}
			};
			final ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
			image = Utils.convertImageToBufferedImage( Toolkit.getDefaultToolkit().createImage(ip) );
			knownImages.put(imgName, image);
		} catch (IOException e) {}
		return image;
	}
	
	// FLOATING FRAMES
	private HashMap<JFrame, FloatingFrameSettings> floatingFrames;
	public void addFloatingFrame(JFrame frame, FloatingFrameSettings settings) {
		floatingFrames.put(frame, settings);
	}
	public void removeFloatingFrame(JFrame frame) {
		floatingFrames.remove(frame);
	}
	
	public void updateFloatingFrames() {
		int x, y;
		FloatingFrameSettings settings;
		for (JFrame frame : floatingFrames.keySet()) {
			settings = floatingFrames.get(frame);
			x = 0;
			y = 0;
			
			switch (settings.pinTo) {
			case Top:
				x = getLocation().x + settings.offsetX(frame);
				y = getLocation().y + settings.offsetY(frame) - frame.getHeight();
				break;
			case Bottom:
				x = getLocation().x + settings.offsetX(frame);
				y = getLocation().y + settings.offsetY(frame) + getHeight();
				break;
			case Left:
				x = getLocation().x + settings.offsetX(frame) - frame.getWidth();
				y = getLocation().y + settings.offsetY(frame);
				break;
			case Right:
				x = getLocation().x + settings.offsetX(frame) + getWidth();
				y = getLocation().y + settings.offsetY(frame);
				break;
			}
			
			frame.setLocation(x, y);
			frame.toFront();
		}
		toFront();
	}
}
