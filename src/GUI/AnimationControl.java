package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JToggleButton;
import javax.swing.Timer;

import GUI.controls.Button;
import GUI.controls.TextField;
import animation.Animation;
import animation.Animation.State;
import animation.Segment;
import body.Body;
import utils.Utils;

@SuppressWarnings("serial")
public class AnimationControl extends FloatingFrame {
	private static AnimationControl instance;
	public static AnimationControl getInstance() {
		if (instance == null)
			instance = new AnimationControl();
		return instance;
	}
	
	public Animation animation;
	
	public JPanel animationPanel;
	public JScrollBar horizontalScrollBar;
	public JScrollBar verticalScrollBar;
	
	public Button prevFrame;
	public Button addBefore;
	public Button delCurrent;
	public Button addAfter;
	public Button nextFrame;
	
	public JToggleButton play;
	public TextField animName;
	
	protected AnimationControl() {
		super(472, 100);
		
		animation = new Animation("");
		animation.length = 100;
		
		
		// create GUI controls
		animationPanel = new JPanel() {
			@Override protected void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g.create();
				Shape oldClip = g2.getClip();
				// draw background
				g2.setColor(Color.decode("#fafafa"));
				g2.fillRect(0, 0, getWidth()-1, getHeight()-1);
				g2.setColor(Color.BLACK);
				g2.drawRect(0, 0, getWidth()-1, getHeight()-1);
				// set clipping rectangle
				g2.setClip(new Rectangle2D.Double(getX()+1,getY()+1,getWidth()-1,getHeight()-1));
				// consider scrollbars positions
				g2.translate(-horizontalScrollBar.getValue(), -verticalScrollBar.getValue());
				// draw animation duration
				for (int x = 0; x < animation.length; x++) {
					if (x == animation.step) 
						g2.setColor(Color.BLUE);
					else
						g2.setColor(Color.BLACK);
					g2.fillRect(5+x*2, 5, 2, 5);
				}
				// draw body segments
				for (int i = 0, y = 0; i < BodyControl.getInstance().segments.getItemCount(); i++) {
					if (BodyControl.getInstance().segments.getSelectedIndex() == i)
						g2.setColor(Color.RED);
					else
						g2.setColor(Color.GREEN);
					y = 20+i*10;
					g2.drawLine(5, y, (animation.length > 0 ? (5+(animation.length-1)*2) : 6), y);
					g2.drawString(BodyControl.getInstance().segments.getItemAt(i), 5, y);
				}
				// draw modificators
				int y = 0;
				for (String ID : animation.segments.keySet()) {
					if (BodyControl.getInstance().segments.getSelectedIndex() == y)
						g2.setColor(Color.RED);
					else
						g2.setColor(Color.GREEN);
					
					for (int x : animation.segments.get(ID).keySet())
						g2.fillRect(5+x*2, 20+y*10, 2, 5);
				}
				// restore old clip
				g2.setClip(oldClip);
				// draw current step
				g2.setColor(Color.BLACK);
				g2.drawRect(5+animation.step*2, 1, 2, getHeight()-2);
				g2.dispose();
			}
		};
		animationPanel.setSize(460, 65);
		
		// MOUSE EVENTS ON PANEL
		MouseAdapter animationPanelMouseAdapter = new MouseAdapter() {
			private boolean dragging = false;
			private int button;
			
			@Override public void mousePressed(MouseEvent e) {
				dragging = true;
				button = e.getButton();
				if (button == MouseEvent.BUTTON3)
					selectAnimationStepByXY(e.getX(), e.getY());
			}
			
			@Override public void mouseReleased(MouseEvent e) {
				dragging = false;
				button = 0;
			}
			
			@Override public void mouseDragged(MouseEvent e) {
				if (dragging && button == MouseEvent.BUTTON3)
					selectAnimationStepByXY(e.getX(), e.getY());
			}
			
			private void selectAnimationStepByXY(int X, int Y) {
				int maxX = animation.length > 0 ? (5+(animation.length-1)*2) : 6;
				if (X > 6 && X < maxX)
					setAnimationStep((X-5)/2);
				else if (X <= 6)
					setAnimationStep(0);
				else if (X > maxX-1)
					setAnimationStep(animation.length > 0 ? animation.length-1 : 0);
			}
		};
		animationPanel.addMouseListener(animationPanelMouseAdapter);
		animationPanel.addMouseMotionListener(animationPanelMouseAdapter);
		
		horizontalScrollBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, 0, 0, 0);
		horizontalScrollBar.setSize(460, 12);
		horizontalScrollBar.setLocation(0, 65);
		
		verticalScrollBar = new JScrollBar(JScrollBar.VERTICAL, 0, 0, 0, 0);
		verticalScrollBar.setSize(12, 65);
		verticalScrollBar.setLocation(460, 0);
		
		
		prevFrame = new Button("<",null,0,80,20,20) {
			@Override public void clicked(ActionEvent e) {
				if (animation != null)
					setAnimationStep(Utils.iterateThrough(animation.step, 0, animation.length-1, -1));
			}
		};
		
		addBefore = new Button("","add",25,80,20,20) {
			HashMap<Integer, Segment> shiftedFrames = new HashMap<>(); // will be used more than once
			@Override public void clicked(ActionEvent e) {
				animation.length += 1;
				// shift all frames from current step to the end
				for (String ID : animation.segments.keySet()) {
					shiftedFrames.clear();
					for (Integer step : animation.segments.get(ID).keySet())
						if (step >= animation.step)
							shiftedFrames.put(step+1, animation.segments.get(ID).get(step));
						else
							shiftedFrames.put(step, animation.segments.get(ID).get(step));
					animation.segments.put(ID, new HashMap<>(shiftedFrames));
				}
				// check if first step
				if (animation.step == 0)
					animation.addFrame(animation.step, Body.getInstance().segments);
				else {
					animation.step -= 1;
					animation.addFrame(animation.step, MainFrame.currentSegment, BodyControl.targetSegment);
				}
			}
		};
		
		delCurrent = new Button("","del",45,80,20,20) {
			HashMap<Integer, Segment> shiftedFrames = new HashMap<>(); // will be used more than once
			@Override public void clicked(ActionEvent e) {
				animation.length -= 1;
				if (animation.length < 0) animation.length = 0;
				// shift all frames from current step to the end
				for (String ID : animation.segments.keySet()) {
					shiftedFrames.clear();
					for (Integer step : animation.segments.get(ID).keySet())
						if (step < animation.step)
							shiftedFrames.put(step, animation.segments.get(ID).get(step));
						else if (step > animation.step)
							shiftedFrames.put(step-1, animation.segments.get(ID).get(step));
					animation.segments.put(ID, new HashMap<>(shiftedFrames));
				}
				animation.step -= 1;
				if (animation.step < 0) animation.step = 0;
			}
		};
		
		addAfter = new Button("","add",65,80,20,20) {
			HashMap<Integer, Segment> shiftedFrames = new HashMap<>(); // will be used more than once
			@Override public void clicked(ActionEvent e) {
				animation.length += 1;
				// shift all frames from current step to the end
				for (String ID : animation.segments.keySet()) {
					shiftedFrames.clear();
					for (Integer step : animation.segments.get(ID).keySet())
						if (step >= animation.step)
							shiftedFrames.put(step+1, animation.segments.get(ID).get(step));
						else
							shiftedFrames.put(step, animation.segments.get(ID).get(step));
					animation.segments.put(ID, new HashMap<>(shiftedFrames));
				}
				// check if last step
				if (animation.step == animation.length-1)
					animation.addFrame(animation.step+1, Body.getInstance().segments);
				else
					animation.addFrame(animation.step+1, MainFrame.currentSegment, BodyControl.targetSegment);
				animation.step += 1;
			}
		};
		
		nextFrame = new Button(">",null,90,80,20,20) {
			@Override public void clicked(ActionEvent e) {
				if (animation != null && animation.length > 1)
					setAnimationStep(Utils.iterateThrough(animation.step, 0, animation.length-1, +1));
			}
		};
		
		play = new JToggleButton("Play");
		play.setBounds(120, 80, 40, 20);
		play.setMargin(new Insets(0, 0, 0, 0));
		play.addItemListener(new ItemListener() {
			@Override public void itemStateChanged(ItemEvent e) {
				animation.state = (e.getStateChange() == ItemEvent.SELECTED) ? State.RUNNING : State.STOPPED;
			}
		});
		
		animName = new TextField("", "Animation name:", 330, 80, 100, 20);
		animName.addKeyListener(new KeyAdapter() {
			@Override public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (animation != null)
						animation.name = animName.getText();
				}
			}
		});
		

		content.add(animationPanel);
		content.add(horizontalScrollBar);
		content.add(verticalScrollBar);
		
		content.add(prevFrame);
		content.add(addBefore);
		content.add(delCurrent);
		content.add(addAfter);
		content.add(nextFrame);

		content.add(play);
		content.add(animName);

		// panel drawing
		Timer timer = new Timer(50, new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				animation.step(Body.getInstance().segments);
				animationPanel.repaint();
			}
		});
		
		finalize();
		timer.start();
	}

	public void editorMouseReleased(MouseEvent e) {
		if (animation.length > 0) {
			animation.addFrame(animation.step, Body.getInstance().segments);
		}
	}
	
	public void updateControls() {
		horizontalScrollBar.setValue(0);
		horizontalScrollBar.setMinimum(0);
		horizontalScrollBar.setMaximum(animation.length);
		verticalScrollBar.setValue(0);
		verticalScrollBar.setMinimum(0);
		verticalScrollBar.setMaximum(Body.getInstance().segments.size()-1);
		animName.setText(animation.name);
		repaint();
	}
	
	public void setAnimationStep(int step) {
		animation.step = step;
		animation.setToSegments(Body.getInstance().segments);
	}
}
