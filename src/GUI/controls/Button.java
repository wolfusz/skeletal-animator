package GUI.controls;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class Button extends JButton {

	private ImageIcon icon;
	private boolean fitIcon;
	
	public Button(String text, String icon, boolean fitIcon, int x, int y, int w, int h) {
		super(text);
		setBounds(x, y, w, h);
		setContentAreaFilled(false);
		setOpaque(false);
		setBorderPainted(false);
		setFocusPainted(false);
		
		this.icon = null;
		if (icon != null && !icon.equals(""))
			this.icon = new ImageIcon(getClass().getResource("/resources/GUI/"+icon+".png"));
		this.fitIcon = fitIcon;
		
		setBackground(Color.WHITE);
		setForeground(Color.BLACK);
		addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { clicked(e); } });
	}
	public Button(String text, String icon, int x, int y, int w, int h) {
		this(text, icon,false, x,y, w,h);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D canvas = (Graphics2D)g.create();
		
		canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		canvas.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		
		canvas.setColor(getBackground());
		canvas.fillOval(0, 0, getWidth()-1, getHeight()-1);

		if (icon != null) {
			if (fitIcon) {
				int w = getWidth()/6;
				int h = getHeight()/6;
				canvas.drawImage(this.icon.getImage(), w, h, getWidth()-2*w, getHeight()-2*h, null);
			} else
				canvas.drawImage(this.icon.getImage(), 0, 0, getWidth(), getHeight(), null);
		}

		if (getModel().isPressed()) {
			canvas.setColor(Color.DARK_GRAY);
		} else if (getModel().isRollover())
			canvas.setColor(Color.WHITE);
		else
			canvas.setColor(Color.GRAY);
		canvas.drawOval(0, 0, getWidth()-1, getHeight()-1);
		
		canvas.setColor(getForeground());
		canvas.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 20));
		canvas.drawString(getText(), getWidth()*1/4, getHeight()*3/4);
		
		canvas.dispose();
	}

	public void clicked(ActionEvent e) {
		// TO OVERRIDE
	}
}
