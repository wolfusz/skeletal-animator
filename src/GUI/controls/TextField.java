package GUI.controls;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JTextField;

@SuppressWarnings("serial")
public class TextField extends JTextField {

	public String message;
	public TextField(String text, String message, int x, int y, int w, int h) {
		super(text);
		setBounds(x, y, w, h);
		this.message = message;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (getText().isEmpty()) {
			g.setColor(Color.DARK_GRAY);
			g.drawString(message, 2, 10);
		}
	}

}
