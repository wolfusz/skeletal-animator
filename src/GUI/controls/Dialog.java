package GUI.controls;

import java.awt.Color;
import java.awt.Insets;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;
import java.util.concurrent.Callable;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import GUI.MainFrame;

public class Dialog {
	private static boolean result;
	private static JDialog dialog;
	
	public static boolean show(JPanel panel, final Callable<Boolean> ok, final Callable<Boolean> cancel) {
		result = false;
		
		dialog = new JDialog();
		dialog.setModalityType(ModalityType.TOOLKIT_MODAL);
		dialog.setTitle("");
		dialog.setSize(panel.getWidth()+10, panel.getHeight()+30);
		dialog.setLocationRelativeTo(MainFrame.getInstance());
		dialog.setUndecorated(true);
		dialog.setShape(new RoundRectangle2D.Double(0, 0, dialog.getWidth(), dialog.getHeight(), 20, 20));
		
		JPanel container = new JPanel();
		container.setSize(dialog.getWidth(), dialog.getHeight());
		container.setBackground(Color.BLACK);
		container.setOpaque(true);
		container.setLayout(null);
		panel.setLocation(5, 5);
		container.add(panel);
		
		
		int buttonWidth = panel.getWidth()/2;
		
		JButton bOK = new JButton("OK");
		bOK.setLocation(5, panel.getHeight()+5);
		bOK.setSize(buttonWidth, 20);
		bOK.setMargin(new Insets(0, 0, 0, 0));
		bOK.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				try {
					if (ok == null)
						result = true;
					else
						result = ok.call();
				} catch (Exception e1) {}
				dialog.setVisible(false);
				dialog.dispose();
			}
		});
		container.add(bOK);
		
		JButton bCancel = new JButton("Cancel");
		bCancel.setLocation(buttonWidth+5, panel.getHeight()+5);
		bCancel.setSize(buttonWidth, 20);
		bCancel.setMargin(new Insets(0, 0, 0, 0));
		bCancel.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				try {
					if (ok == null)
						result = false;
					else
						result = cancel.call();
				} catch (Exception e1) {}
				dialog.setVisible(false);
				dialog.dispose();
			}
		});
		container.add(bCancel);
		
		dialog.add(container);
		dialog.setVisible(true);
		
		return result;
	}
}
