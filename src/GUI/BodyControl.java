package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

import GUI.controls.Button;
import GUI.dialog.SegmentCreationDialog;
import GUI.dialog.SetParentDialog;
import animation.Segment.Direction;
import body.Body;
import body.Segment;
import utils.Point;

@SuppressWarnings("serial")
public class BodyControl extends FloatingFrame {
	private static BodyControl instance = null;
	public static BodyControl getInstance() {
		if (instance == null)
			instance = new BodyControl();
		return instance;
	}

		
	public Button add;
	public Button del;
	public JButton setParent;
	public JCheckBox IK;
	public JTextField image;
	public JTextField angle;
	public JTextField length;
	private JPanel panel;
	
	public static Segment targetSegment;
	public JComboBox<String> segments;
	public JComboBox<Direction> direction;
	
	protected BodyControl() {
		super(100, 200);
		
		segments = new JComboBox<>();
		segments.setLocation(20, 0);
		segments.setSize(60, 20);
		segments.addItemListener(new ItemListener() { @Override public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				MainFrame.currentSegment = (String)e.getItem();
				targetSegment = Body.getInstance().get(MainFrame.currentSegment);
				image.setText(targetSegment.image);
			}
		}});
		content.add(segments);
		
		add = new Button("","add", 0,0, 20,20) {
			@Override public void clicked(ActionEvent e) {
				if (SegmentCreationDialog.show() == true) {
					Segment added = Body.getInstance().add(
							SegmentCreationDialog.getID(), (String)segments.getSelectedItem(),
							image.getText(),
							MainFrame.position.x, MainFrame.position.y,
							SegmentCreationDialog.getLength());
					added.position(Body.getInstance().get(added.parent));
				}
			}
		};
		content.add(add);
		
		del = new Button("","del", 80,0, 20,20) {
			@Override public void clicked(ActionEvent e) {
				Body.getInstance().remove((String) segments.getSelectedItem());
			}
		};
		content.add(del);
		
		setParent = new JButton("Set parent");
		setParent.setLocation(40, 20);
		setParent.setSize(60, 20);
		setParent.setMargin(new Insets(0, 0, 0, 0));
		setParent.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				if (SetParentDialog.show() == true) {
					Segment current = Body.getInstance().get((String)segments.getSelectedItem());
					if (current != null)
						if (SetParentDialog.getValue().equals("None")) {
							current.parent = null;
						} else {
							current.parent = SetParentDialog.getValue();
						}
				}
			}
		});
		content.add(setParent);
		
		IK = new JCheckBox("IK");
		IK.setSize(40, 20);
		IK.setLocation(0, 20);
		IK.setBackground(null);
		IK.setToolTipText("Inverse Kinematics");
		content.add(IK);
		
		image = new JTextField() {
			@Override protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				if (getText().isEmpty()) {
					g.setColor(Color.GRAY);
					g.drawString("Image:", 5, getHeight()*3/4);
				}
			}
		};
		image.setBounds(0, 40, 60, 20);
		image.addKeyListener(new KeyAdapter() {
			@Override public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (targetSegment != null)
						targetSegment.image = image.getText();
				}
				super.keyPressed(e);
			}
		});
		image.setToolTipText("Image");
		content.add(image);

		angle = new JTextField() {
			@Override protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				if (getText().isEmpty()) {
					g.setColor(Color.GRAY);
					g.drawString("Angle:", 5, getHeight()*3/4);
				}
			}
		};
		angle.setBounds(0, 60, 40, 20);
		angle.addMouseWheelListener(new MouseWheelListener() {
			@Override public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() < 0) {
					targetSegment.angle += 1f;
				} else {
					targetSegment.angle -= 1f;
				}
			}
		});
		angle.addKeyListener(new KeyAdapter() {
			@Override public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (targetSegment != null) {
						float a = Integer.parseInt(angle.getText());
						float delta = a - targetSegment.angle;
						targetSegment.angle = a;
						Body.getInstance().get("placeHolder").updateChildrens(delta);
					}
				}
				super.keyPressed(e);
			}
		});
		angle.setToolTipText("Angle");
		content.add(angle);

		length = new JTextField() {
			@Override protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				if (getText().isEmpty()) {
					g.setColor(Color.GRAY);
					g.drawString("Length:", 5, getHeight()*3/4);
				}
			}
		};
		length.setBounds(0, 80, 40, 20);
		length.addMouseWheelListener(new MouseWheelListener() {
			@Override public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() < 0) {
					targetSegment.length += 1f;
				} else {
					targetSegment.length -= 1f;
				}
			}
		});
		length.addKeyListener(new KeyAdapter() {
			@Override public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (targetSegment != null) {
						targetSegment.length = Integer.parseInt(length.getText());
						Body.getInstance().get("placeHolder").updateChildrens();
					}
				}
				super.keyPressed(e);
			}
		});
		length.setToolTipText("Length");
		content.add(length);
		
		direction = new JComboBox<>();
		direction.setBounds(50, 70, 50, 20);
		direction.addItem(Direction.Right);
		direction.addItem(Direction.Left);
		direction.setSelectedIndex(0);
		direction.addItemListener(new ItemListener() {
			@Override public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
				}
			}
		});
		content.add(direction);
		
		panel = new JPanel() {
			@Override protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2 = (Graphics2D)g.create();
				g2.setColor(Color.WHITE);
				g2.fill(getBounds());

				g2.translate(panel.getWidth()/2, panel.getHeight()/2);
				g2.scale(0.4, 0.4);
				
				g2.setColor(Color.BLACK);
				Body.getInstance().draw(g2);
				
				if (targetSegment != null) {
					g2.setColor(Color.RED);
					g2.fillOval(targetSegment.getPos().x-5, targetSegment.getPos().y-5, 10, 10);					
				}
				
				g2.dispose();
			}
		};
		panel.setLocation(0, 100);
		panel.setSize(100, 100);
		content.add(panel);
		
		// MOUSE EVENTS
		panel.addMouseListener(new MouseAdapter() {
			@Override public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					// convert clicked point to location in main frame
					Point clickedPoint = new Point(e.getX()-panel.getWidth()/2, e.getY()-panel.getHeight()/2);
					double scaleX = (double)panel.getWidth() / (double)MainFrame.getInstance().panel.getWidth();
					double scaleY = (double)panel.getHeight() / (double)MainFrame.getInstance().panel.getHeight();
					clickedPoint.x = (int)(clickedPoint.x * (1/scaleX));
					clickedPoint.y = (int)(clickedPoint.y * (1/scaleY));
//					System.out.println("----- "+new Time(e.getWhen()));
//					System.out.println("FrameBody X,Y: " + panel.getWidth() +","+ panel.getHeight());
//					System.out.println("FrameMain X,Y: " + FrameMain.getInstance().panel.getWidth() +","+ FrameMain.getInstance().panel.getHeight());
//					System.out.println("Scale X,Y: " + scaleX +","+ scaleY);
//					System.out.println("Event X,Y: " + e.getX() +","+ e.getY());
//					System.out.println("Point X,Y: " + clickedPoint.getX() +","+ clickedPoint.getY());
					
					targetSegment = Body.getInstance().getClosestSegment(clickedPoint);
					if (targetSegment != null)
						BodyControl.getInstance().segments.setSelectedItem(Body.getInstance().getID(targetSegment));
				}
			}
		});
		
		// TIMER
		Timer timer = new Timer(50, new ActionListener() {
			double oldAngle;
			int oldLength;
			@Override public void actionPerformed(ActionEvent e) {
				if (targetSegment != null) {
					if (oldAngle != targetSegment.angle || oldLength != targetSegment.length) {
						angle.setText(""+targetSegment.angle);
						angle.setSelectionStart(0);
						angle.setSelectionEnd(0);
						length.setText(""+targetSegment.length);
						length.setSelectionStart(0);
						length.setSelectionEnd(0);
					}
					oldAngle = targetSegment.angle;
					oldLength = targetSegment.length;
				}
				repaint();
			}
		});
		
		finalize();
		timer.start();
	}
}
