package body;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class Utils {

	public static BufferedImage convertImageToBufferedImage(Image src) {
	    if (src instanceof BufferedImage)
	        return (BufferedImage) src;
		BufferedImage bi = new BufferedImage(src.getWidth(null), src.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bi.createGraphics();
		g.drawImage(src, 0, 0, null);
		g.dispose();
		return bi;
	}
	
	public static BufferedImage resizeImage(BufferedImage src, int width, int height) {
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bi.createGraphics();
		g.drawImage(src, 0, 0, width, height, null);
		g.dispose();
		return bi;
	}

}
