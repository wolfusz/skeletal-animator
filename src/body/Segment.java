package body;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import utils.Point;

public class Segment {
	public static final double DEGTORAD = Math.PI/180;
	public static final double RADTODEG = 180/Math.PI;
	
	public String parent;
	public String image;
	public int x;
	public int y;
	public int length;
	public float angle;
	
	public Segment(String parent, String image, int x, int y, int length, float angle) {
		this.parent = parent;
		this.image = image;
		this.x = x;
		this.y = y;
		this.length = length;
		this.angle = angle;
	}
	public Segment(String parent, String image, int x, int y, int length) {
		this(parent,image,x,y,length,0f);
	}
	
	public boolean hasParent() {
		return !(parent == null || parent.isEmpty());
	}
	
	public AffineTransform getTransform() {
		AffineTransform t = new AffineTransform();
		t.translate(x, y);
		t.rotate(angle * DEGTORAD);
		return t;
	}
	
	public Point getPos() {
		return new Point(x, y);
	}
	
	public Point getPin() {
		double r = angle * DEGTORAD;
		double x = this.x + Math.cos(r) * length;
		double y = this.y + Math.sin(r) * length;
		return new Point((int)x, (int)y);
	}
	
	public Float pointAt(Point target) {
		if (target == null) return null;
		int dx = target.x - x;
		int dy = target.y - y;
		angle = (float) (Math.atan2(dy, dx) * RADTODEG);
		return angle;
	}
	
	public void position(Segment t) {
		x = t.getPin().x;
		y = t.getPin().y;
	}
	
	public Point reach(Point target) {
		if (target == null) return null;
		pointAt(target);
		int w = getPin().x - x;
		int h = getPin().y - y;
		return new Point(target.x - w, target.y - h);
	}
	
	public Float IK(Segment B, Point target) {
		if (B == null) return null;
		target = reach(target);
		B.IK(target);
		position(B);
		return angle;
	}
	
	public Float IK(Point target) {
		if (hasParent())
			return IK(Body.getInstance().get(parent), target);
		return pointAt(target);
	}
	
	public void draw(Graphics2D g) {
		g.setColor(Color.BLACK);
		
		AffineTransform t = g.getTransform();
		g.transform(getTransform());
//		g.drawImage(image, -image.getHeight()/2, -image.getHeight()/2, image.getWidth()+image.getHeight(), image.getHeight(), null);
		g.fillRoundRect(-5, -5, length+10, 10, 10, 10);
		g.setTransform(t);
	}

	public void updateChildrens() { updateChildrens(0); }
	public void updateChildrens(double deltaAngle) {
		for (Segment s : Body.getInstance().getSegments())
			if (s.hasParent() && s.parent.equals(Body.getInstance().getID(this))) {
				s.position(this);
				s.angle += deltaAngle;
				s.updateChildrens(deltaAngle);
			}
	}
}
