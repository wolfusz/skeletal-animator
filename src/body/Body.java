package body;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Set;

import GUI.BodyControl;
import GUI.MainFrame;
import animation.Animation;
import utils.Point;

public class Body {
	private static Body instance = null;
	public static Body getInstance() {
		if (instance == null)
			instance = new Body();
		return instance;
	}
	
	public LinkedHashMap<String,Segment> segments;
	public Animation animation;
	
	public Body() {
		animation = null;
		segments = new LinkedHashMap<>();
	}
	
	// LOADING/SAVING FROM/TO FILE
//	@SuppressWarnings("unchecked")
//	public void loadFromFile(String path) {
//		LinkedList<String> toRead = null;
//		
//		FileInputStream fout;
//		ObjectInputStream oos;
//		try {
//			fout = new FileInputStream(path);
//			oos = new ObjectInputStream(fout);
//			toRead = (LinkedList<String>) oos.readObject();
//			oos.close();
//			fout.close();
//		} catch (Exception e) {}
//
//		segments.clear();
//		if (toRead != null) {
//			SegmentInfo segmentInfo;
//			Segment segment;
//			for (String info : toRead) {
//				segmentInfo = SegmentInfo.fromString(info);
//				segment = new Segment(
//					segmentInfo.parent,
//					segmentInfo.x,
//					segmentInfo.y,
//					segmentInfo.length,
//					segmentInfo.angle
//				);
//				segments.put(segmentInfo.ID, segment);
//			}
//		}
//	}
//	
//	public void saveToFile(String path) {
//		LinkedList<String> toSave = new LinkedList<>();
//		SegmentInfo segmentInfo;
//		for (Segment segment : segments.values()) {
//			segmentInfo = new SegmentInfo(
//					getIDof(segment),
//					segment.parent,
//					segment.x,
//					segment.y,
//					segment.length,
//					segment.angle
//				);
//			toSave.add(segmentInfo.toString());
//		}
//
//		FileOutputStream fout;
//		ObjectOutputStream oos;
//		try {
//			fout = new FileOutputStream(path);
//			oos = new ObjectOutputStream(fout);
//			oos.writeObject(toSave);
//			oos.close();
//			fout.close();
//		} catch (Exception e) {}
//	}
	
	
	public Segment add(String ID, String parent, String image, int x, int y, int length, float angle) {
		if (segments.containsKey(ID))
			return null;
		segments.put(ID, new Segment(parent, image, x, y, length, angle));
		BodyControl.getInstance().segments.addItem(ID);
		return get(ID);
	}
	public Segment add(String ID, String parent, String image, int x, int y, int length) {
		return add(ID,parent,image,x,y,length,0);
	}
	
	public boolean remove(String ID) {
		BodyControl.getInstance().segments.removeItem(ID);
		return segments.remove(ID) != null;
	}
	
	
	public Segment get(String ID) {
		return segments.get(ID);
	}
	
	public Collection<Segment> getSegments() {
		return segments.values();
	}
	
	public Set<String> getIDs() {
		return segments.keySet();
	}

	public String getID(Segment segment) {
        for (String ID : segments.keySet())
            if (segments.get(ID).equals(segment))
                return ID;
        return null;
	}


	public Point getClosestPin(Point p) {
		for (Segment s : getSegments())
			if (s.getPin().distance(p) < 5)
				return s.getPin();
		return null;
	}

	public Point getClosestPos(Point p) {
		for (Segment s : getSegments())
			if (s.getPos().distance(p) < 10)
				return s.getPin();
		return null;
	}

	public Segment getClosestSegment(Point p) {
		for (Segment s : getSegments()) {
			System.out.println(getID(s) + ": "+ s.getPin().distance(p));
			if (s.getPin().distance(p) < 100)
				return s;
		}
		return null;
	}

	
    public void draw(Graphics2D canvas) {
    	canvas.setColor(Color.BLACK);

        // save starting matrix
        AffineTransform t = canvas.getTransform();

        BufferedImage image;
        for (Segment segment : getInstance().getSegments()) {
        	if (segment.image == null || segment.image.isEmpty()) continue;
            image = MainFrame.getInstance().getImage(segment.image);

            // move to segments position
            canvas.translate(segment.x, segment.y);
            canvas.rotate(segment.angle * Segment.DEGTORAD);
            
            if (image != null) {
                // draw image including offset
                int offset = image.getHeight()/2;
                canvas.drawImage(image, -offset, -offset, segment.length + image.getHeight(), image.getHeight(), null);
            }
            
            canvas.setColor(Color.red);
            canvas.drawLine(0, 0, segment.length, 0);

            // restore starting matrix
            canvas.setTransform(t);
        }
    }
	
	
	public void playAnim(String animName) {
		animation = new Animation(MainFrame.getDir("animation")+animName+".anim");
        animation.state = Animation.State.RUNNING;
	}

    public void update() {
        if (animation != null)
            animation.step(segments);
    }
}
