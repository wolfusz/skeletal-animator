package body;

public class SegmentInfo {
	public String ID;
	public String parent;
	public int x;
	public int y;
	public int length;
	public double angle;
	
	public SegmentInfo(String ID, String parent, int x, int y, int length, double angle) {
		this.ID = ID;
		this.parent = parent;
		this.x = x;
		this.y = y;
		this.length = length;
		this.angle = angle;
	}
	public SegmentInfo() {
		this("","",0,0,0,0.0);
	}
	
	public static SegmentInfo fromString(String info) {
		String[] fields = info.split(",");
		if (fields.length == 7)
			return new SegmentInfo(
				fields[0],
				fields[1],
				Integer.parseInt(fields[3]),
				Integer.parseInt(fields[4]),
				Integer.parseInt(fields[5]),
				Double.parseDouble(fields[6])
			);
		return null;
	}
	
	public String toString() {
		return ID+","+parent+","+x+","+y+","+length+","+angle;
	}
}
