package utils;

public class Point {
	public int x;
	public int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

    public Point add(int a) { return new Point(this.x + a, this.y + a); }
    public Point sub(int a) { return new Point(this.x - a, this.y - a); }
    public Point mul(int a) { return new Point(this.x * a, this.y * a); }
    public Point div(int a) { return new Point(this.x / a, this.y / a); }

    public Point add(int x, int y) { return new Point(this.x + x, this.y + y); }
    public Point sub(int x, int y) { return new Point(this.x - x, this.y - y); }
    public Point mul(int x, int y) { return new Point(this.x * x, this.y * y); }
    public Point div(int x, int y) { return new Point(this.x / x, this.y / y); }

    public Point add(Point p) { return new Point(this.x + p.x, this.y + p.y); }
    public Point sub(Point p) { return new Point(this.x - p.x, this.y - p.y); }
    public Point mul(Point p) { return new Point(this.x * p.x, this.y * p.y); }
    public Point div(Point p) { return new Point(this.x / p.x, this.y / p.y); }

    public Double distance(Point A) {
        if (A == null) return null;
        int dx = this.x - A.x;
        int dy = this.y - A.y;
        return Math.sqrt(dx*dx + dy*dy);
    }
    public Point distanceXY(Point A) {
        if (A == null) return null;
        return new Point(
        		this.x - A.x,
        		this.y - A.y
    		);
    }
}
