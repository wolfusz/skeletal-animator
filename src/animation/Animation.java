package animation;

import java.util.HashMap;
import java.util.Iterator;

import animation.Segment.Direction;

public class Animation {
    public enum State {
        STOPPED,
        RUNNING
    }

    public State state;
    public String name;
    public int length;
    public int step;
    public HashMap<String,HashMap<Integer,Segment>> segments;
	
	public Animation(String animName) {
		state = State.STOPPED;
		name = animName;
		
		length = 0;
		step = 0;
		segments = new HashMap<>();
	}
	
	public void addFrame(int step, String ID, body.Segment segment) {
		// check if ID initialized
		if (segments.get(ID) == null)
			segments.put(ID, new HashMap<>());
		// add segment to animation
		segments.get(ID).put(step, Segment.fromBodySegment(segment));
	}
	public void addFrame(int step, HashMap<String, body.Segment> segments) {
		for (String ID : segments.keySet())
			addFrame(step, ID, segments.get(ID));
	}
	
	public void setToSegments(HashMap<String, body.Segment> segments) {
        Segment modificator, modificator2;
        body.Segment toChange;
        Iterator<Integer> iterator;
		int iteratorValue, firstStep = 0;
		float stepValue;
		
        for (String ID : segments.keySet()) {
        	toChange = segments.get(ID);
        	if (toChange != null && this.segments.get(ID) != null) {
            	modificator = this.segments.get(ID).get(step);
            	
            	if (modificator != null) {
            		// change values
                    toChange.length = modificator.length;
                    toChange.angle = modificator.angle;
                    
            	} else {
            		
            		iterator = this.segments.get(ID).keySet().iterator();
            		while (iterator.hasNext()) {
            			iteratorValue = iterator.next();
        				if (this.segments.get(ID).get(iteratorValue) != null) {
        					
                			if (iteratorValue < step) {
                				// get first modificator
            					modificator = this.segments.get(ID).get(iteratorValue);
            					firstStep = iteratorValue;
            					
                			} else if (iteratorValue > step) {
                				// get second modificator
                				modificator2 = this.segments.get(ID).get(iteratorValue);
                				
                				// calculate step value
                				stepValue = ((float)(step - firstStep)/(float)(iteratorValue - firstStep)) * (modificator.direction == Direction.Left ? -1f : 1f);
                				
                				// change values
                				toChange.length = (int) (modificator.length + ((modificator2.length - modificator.length) * stepValue));
                				toChange.angle = modificator.angle + ((modificator2.angle - modificator.angle) * stepValue);
                				
//                				if (BodyControl.getInstance().segments.getSelectedItem().equals(ID)) {
//                					System.out.println("###########");
//                					System.out.println(Segment.fromBodySegment(toChange).toString());
//                					System.out.println("stepValue: " + stepValue);
//                					System.out.println("calculation: " + ((double)(step - firstStep)/(double)(iteratorValue - firstStep)));
//                					System.out.println("delta: " + (modificator2.angle - modificator.angle));
//                					System.out.println("1: " + modificator.angle);
//                					System.out.println("2: " + modificator2.angle);
//                					System.out.println("step: " + step);
//                					System.out.println("firstStep: " + firstStep);
//                					System.out.println("iteratorValue: " + iteratorValue);
//                				}
                				
                				// we found second modificator, so exit loop
                				break;
            				}
            			}
            		}
            	}        		
        	}
        }
        segments.get("placeHolder").updateChildrens(0);
	}
	
	public void step(HashMap<String, body.Segment> segments) {
        if (state == State.RUNNING) {
        	setToSegments(segments);
            if (length > 0)
            	step = (step + 1) % length;
        }
	}
}
