package animation;

import java.io.Serializable;
import java.util.HashMap;

@SuppressWarnings("serial")
public class Frame implements Serializable {
	public HashMap<String,Segment> segments;
	
	public Frame() {
		this.segments = new HashMap<>();
	}
	
	public void addSegment(String segment) {
		this.segments.put(
				segment.substring(0, segment.indexOf(":")), // ID
				Segment.fromString(segment)					// segment
			);
	}

    public static Frame fromString(String input) {
        Frame frame = new Frame();
        String[] segments = input.split(";");
        for (String segment : segments)
        	frame.addSegment(segment);
        return frame;
    }

    public String toString() {
        String result = "";
        Segment segment;
        for (String ID : segments.keySet()) {
        	segment = segments.get(ID);
            if (result.isEmpty())
                result = ID+":"+segment.toString();
            else
                result += ";" + ID+":"+segment.toString();
        }
        return result;
    }
}
