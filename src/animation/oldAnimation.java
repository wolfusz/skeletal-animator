package animation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.sun.glass.ui.Application;

public class oldAnimation {
    public enum State {
        STOPPED,
        RUNNING
    }

    public State state;
    private String name;
    private LinkedList<Frame> frames;
    private int currentFrame;
	
	public oldAnimation(String animName) {
		state = State.STOPPED;
		name = animName;
		
		currentFrame = 0;
		frames = new LinkedList<>();		
		if (animName != null && !animName.isEmpty()) {
			animName = Application.GetApplication().getDataDirectory() + File.separatorChar + "animation" + File.separatorChar + animName;
			System.out.println("Loading... " + animName);
			try {
	            BufferedReader r = new BufferedReader( new InputStreamReader( new FileInputStream(animName) ) );

	            String line;
	            // read line and create frame from it
	            while ((line = r.readLine()) != null)
	                frames.add( Frame.fromString(line) );

	            r.close();
	        } catch (Exception ignored) {
	            // nothing to do here
	        }
		}
	}
	
	public void setName(String name) { this.name = name; }
	public String getName() { return name; }
	public LinkedList<Frame> getFrames() { return frames; }
	
	public void step(HashMap<String, body.Segment> segments) {
        if (state == State.RUNNING) {
            body.Segment toChange;
            for (Entry<String,Segment> segment : frames.get(currentFrame).segments.entrySet()) {
                toChange = segments.get(segment.getKey());
                toChange.x = segment.getValue().x;
                toChange.y = segment.getValue().y;
                toChange.length = segment.getValue().length;
                toChange.angle = segment.getValue().angle;
            }
            currentFrame = (currentFrame + 1) % frames.size();
        }
	}

	public void save() {
		String animName = Application.GetApplication().getDataDirectory() + File.separatorChar + "animation" + File.separatorChar + name;
		try {
            BufferedWriter w = new BufferedWriter( new OutputStreamWriter( new FileOutputStream( animName ) ) );
            for (Frame frame : frames)
            	w.write(frame.toString() + "\n");
            w.close();
        } catch (Exception ignored) {
            // nothing to do here
        }
	}
}
