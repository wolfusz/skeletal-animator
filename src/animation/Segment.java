package animation;

public class Segment {
//    public int x;
//    public int y;
	public enum Direction {
		Left, Right
	};
	
    public int length;
    public float angle;
    public Direction direction; // true - left, false - right

	public Segment(/*int x, int y, */int length, float angle, Direction direction) {
//		this.x = x;
//		this.y = y;
		this.length = length;
		this.angle = angle;
		this.direction = direction;
	}
	public Segment() {
		this(/*0,0,*/0,0f,Direction.Right);
	}
	
	public static Segment fromBodySegment(body.Segment segment) {
		return new Segment(/*segment.x, segment.y, */segment.length, segment.angle, Direction.Right);
	}
	
	public static Segment fromString(String info) {
		String[] fields = info.split(",");
		if (fields.length == 3)//4)
			return new Segment(
//				Integer.parseInt(fields[0]),
//				Integer.parseInt(fields[1]),
				Integer.parseInt(fields[2]),
				Float.parseFloat(fields[3]),
				(fields[4].equals("L") ? Direction.Left : Direction.Right)
			);
		return null;
	}

    public String toString() {
        return /*x+","+y+","+*/length+","+angle+","+(direction == Direction.Left ? "L" : "R");
    }
}
